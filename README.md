# tex-template

## Description
generalized tex template that can be used by students for their assignments

## Compilation
You will need texlive to compile this project. I recommend using lualatex with
latexmk

~~~
latexmk -norc -lualatex <file.tex> 
~~~

## Contributing
Feel free to create merge requests or open issues :) 

## Authors and acknowledgment
big thanks to ljrk0 for the base of this template (and everything else). Also check out this [repository](https://git.imp.fu-berlin.de/koenigl/tex-templates).

## Project status
Project is live at least in this semester (SS 22), it is intended to be
maintained further on. If you want to take it over leave me a message. 
